package gophsm

import (
	"errors"
	"fmt"
	"os"
	"runtime"
	"sync"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// ErrEventRejected is the error returned when the state machine cannot process
// an event in the state that it is in.
var ErrEventRejected = errors.New("event rejected")

type StateMachine struct {
	sync.Mutex
	Name               string
	initStateSet       bool
	CurrentState       string
	States             map[string]State
	Context            interface{}
	OnStateChangeFuncs map[int]func(currentState string, event string, resultingState string, fsm *StateMachine) // OnStateChangeFuncs describes functions that run on every successfull state change
}

func (s *StateMachine) Init(name string) {
	s.Name = name
	s.States = make(map[string]State)
	s.initStateSet = false
	s.OnStateChangeFuncs = make(map[int]func(currentState string, event string, resultingState string, fsm *StateMachine))
}

func (s *StateMachine) GetCurrentState() string {
	s.Lock()
	defer s.Unlock()
	return s.CurrentState
}

func (s *StateMachine) NewState(state string) error {
	s.Lock()
	defer s.Unlock()
	if _, ok := s.States[state]; ok {
		return fmt.Errorf("%s: State \"%s\" already exists", s.Name, state)
	}
	var ns State
	ns.Events = make(map[string]string)
	ns.InFuncs = make(map[int]func(currentState string, event string, resultingState string, fsm *StateMachine) error)
	ns.Funcs = make(map[int]func(currentState string, lastEvent string, fsm *StateMachine) error)
	ns.OutFuncs = make(map[int]func(currentState string, event string, resultingState string, fsm *StateMachine) error)
	s.States[state] = ns
	return nil
}

func (s *StateMachine) NewEvent(CurrentState string, Event string, ResultingState string) error {
	s.Lock()
	defer s.Unlock()
	if _, ok := s.States[CurrentState]; !ok {
		fmt.Errorf("%s: State \"%s\" does not exist", s.Name, CurrentState)
	}
	if _, ok := s.States[ResultingState]; !ok {
		fmt.Errorf("%s: State \"%s\" does not exist", s.Name, ResultingState)
	}
	s.States[CurrentState].Events[Event] = ResultingState
	return nil
}

func (s *StateMachine) SetDefaultState(state string) error {
	s.Lock()
	if s.initStateSet {
		return fmt.Errorf("%s: Default State already set", s.Name)
	}
	if _, ok := s.States[state]; !ok {
		fmt.Errorf("%s: State \"%s\" does not exist", s.Name, state)
	}
	s.CurrentState = state
	s.initStateSet = true
	s.Unlock()
	return nil
}

type State struct {
	Events   map[string]string                                                                               // read: map[Event]ResultingState
	InFuncs  map[int]func(currentState string, event string, resultingState string, fsm *StateMachine) error // InFunc describes functions that need to run without error fot the state to be successfully changed
	Funcs    map[int]func(currentState string, lastEvent string, fsm *StateMachine) error                    // Func describes functions that need to run without error fot the state to be successfully changed
	OutFuncs map[int]func(currentState string, event string, resultingState string, fsm *StateMachine) error // OutFunc describes functions that need to run without error fot the state to be successfully changed
	Context  interface{}
}

func (s *StateMachine) SendEvent(event string) (err error) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	//MultiWriter
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})

	if s.CurrentState == "" {
		log.Printf("%s: Error: CurrentState not set", s.Name)
		return fmt.Errorf("%s: CurrentState not set", s.Name)
	}
	s.Lock()
	defer s.Unlock()
	if CurrentState, ok := s.States[s.CurrentState]; ok {
		pc, fn, line, _ := runtime.Caller(1)
		if ResultingState, ok := CurrentState.Events[event]; ok {
			log.Printf("%s: Changing State from \"%s\" to \"%s\" due to event \"%s\" - sent from %s[%s:%d]", s.Name, s.CurrentState, ResultingState, event, runtime.FuncForPC(pc).Name(), fn, line)
			// Run OutFuncs --> must not have an error
			for i := 0; i < len(s.States[s.CurrentState].OutFuncs); i++ {
				log.Printf("%s: Running OutFunc nr. \"%d\" to transition from \"%s\"", s.Name, i, s.CurrentState)
				err := s.States[s.CurrentState].OutFuncs[i](s.CurrentState, event, ResultingState, s)
				if err != nil {
					log.Printf("%s: Error: Cannot change State from \"%s\" to \"%s\": failed OutFunc nr. \"%d\"", s.Name, s.CurrentState, ResultingState, i)
					return fmt.Errorf("%s: Error: Cannot change State from \"%s\" to \"%s\": failed OutFunc nr. \"%d\"", s.Name, s.CurrentState, ResultingState, i)
				}
			}
			// Run in funcs --> must not have an error
			for i := 0; i < len(s.States[ResultingState].InFuncs); i++ {
				log.Printf("%s: Running InFunc nr. \"%d\" to transition into \"%s\"", s.Name, i, ResultingState)
				err := s.States[ResultingState].InFuncs[i](s.CurrentState, event, ResultingState, s)
				if err != nil {
					log.Printf("%s: Error: Cannot change State from \"%s\" to \"%s\": failed InFunc nr. \"%d\"", s.Name, s.CurrentState, ResultingState, i)
					return fmt.Errorf("%s: Error: Cannot change State from \"%s\" to \"%s\": failed InFunc nr. \"%d\"", s.Name, s.CurrentState, ResultingState, i)
				}
			}
			s.CurrentState = ResultingState
			// Run general state change funcs --> errors ignored
			for i := 0; i < len(s.OnStateChangeFuncs); i++ {
				log.Printf("%s: Running OnStateChangeFuncs nr. \"%d\" to transition into \"%s\"", s.Name, i, ResultingState)
				s.OnStateChangeFuncs[i](s.CurrentState, event, ResultingState, s)
			}
			// Run in-state funcs --> errors logged only
			go func(CurrentState string, event string, s *StateMachine) {
				for i := 0; i < len(s.States[CurrentState].Funcs); i++ {
					log.Printf("%s: Running Func nr. \"%d\" in state \"%s\"", s.Name, i, CurrentState)
					err := s.States[CurrentState].Funcs[i](CurrentState, event, s)
					if err != nil {
						log.Printf("%s: Error: Got error from \"%s\" Func nr. \"%d\"", s.Name, CurrentState, i)
					}
				}
			}(ResultingState, event, s)
			return nil
		}
		log.Printf("%s: Error: Cannot change State from \"%s\" to some other due to event \"%s\" - sent from %s[%s:%d]", s.Name, s.CurrentState, event, runtime.FuncForPC(pc).Name(), fn, line)
		return ErrEventRejected
	}
	return nil
}
