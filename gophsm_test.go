package gophsm

import (
	"fmt"
	"testing"

	"github.com/rs/zerolog/log"
)

func TestMotorStateMachine(t *testing.T) {
	var motorFsm StateMachine
	motorFsm.Init("motorFSM")

	motorFsm.NewState("Off")
	motorFsm.NewState("Fwd")
	motorFsm.NewState("Rwd")
	motorFsm.SetDefaultState("Off")

	motorFsm.NewEvent("Off", "GoFwd", "Fwd")
	motorFsm.NewEvent("Off", "GoRwd", "Rwd")
	motorFsm.NewEvent("Fwd", "Stop", "Off")
	motorFsm.NewEvent("Rwd", "Stop", "Off")

	motorFsm.States["Fwd"].InFuncs[0] = func(currentState string, event string, resultingState string, fsm *StateMachine) error {
		log.Print("Entering " + resultingState)
		return nil
	}

	// Set the initial "off" state in the state machine.
	err := motorFsm.SendEvent("Off")
	if err == nil {
		t.Errorf("Could Set CurrentState Again: %s", err)
	}

	err = motorFsm.SendEvent("GoFwd")
	if err != nil {
		t.Errorf("Cannot set state: %s", err)
	}

	motorFsm.States["Fwd"].InFuncs[1] = func(currentState string, event string, resultingState string, fsm *StateMachine) error {
		log.Print("Entering Fwd, generating error..")
		return fmt.Errorf("Test Error..")
	}

	err = motorFsm.SendEvent("GoFwd")
	if err == nil {
		t.Errorf("Can set state despite failing InFunc")
	}

	err = motorFsm.SendEvent("GoRwd")
	if err == nil {
		t.Errorf("Can change state directly: %s", err)
	}

	err = motorFsm.SendEvent("Stop")
	if err != nil {
		t.Errorf("Cannot set state: %s", err)
	}

	err = motorFsm.SendEvent("GoRwd")
	if err != nil {
		t.Errorf("Cannot set state: %s", err)
	}

}
